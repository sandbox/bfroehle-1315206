<?php
// $Id$

/**
 * @file
 * Implement a resolution field, based on the file module's file field.
 */

/**
 * Implements hook_field_info().
 */
function resolution_field_info() {
  return array(
    'resolution' => array(
      'label' => t('Resolution'),
      'description' => t('This field stores the ID of an resolution file as an integer value.'),
      'settings' => array(
        'display_field' => 1,
        'uri_scheme' => variable_get('file_default_scheme', 'public'),
      ),
      'instance_settings' => array(
        'file_extensions' => 'pdf',
        'file_directory' => '',
        'max_filesize' => '',
      ),
      'default_widget' => 'resolution_resolution',
      'default_formatter' => 'resolution',
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function resolution_field_settings_form($field, $instance, $has_data) {
  $defaults = field_info_field_settings($field['type']);
  $settings = array_merge($defaults, $field['settings']);

  $form['#attached']['js'][] = drupal_get_path('module', 'file') . '/file.js';

  $form['display_field'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable <em>Display</em> field'),
    '#default_value' => $settings['display_field'],
    '#description' => t('The display option allows users to choose if a file should be shown when viewing the content.'),
  );

  $scheme_options = array();
  foreach (file_get_stream_wrappers(STREAM_WRAPPERS_WRITE_VISIBLE) as $scheme => $stream_wrapper) {
    $scheme_options[$scheme] = $stream_wrapper['name'];
  }
  $form['uri_scheme'] = array(
    '#type' => 'radios',
    '#title' => t('Upload destination'),
    '#options' => $scheme_options,
    '#default_value' => $settings['uri_scheme'],
    '#description' => t('Select where the final files should be stored. Private file storage has significantly more overhead than public files, but allows restricted access to files within this field.'),
    '#disabled' => $has_data,
  );

  return $form;
}

/**
 * Implements hook_field_instance_settings_form().
 */
function resolution_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];

  // Use the file field instance settings form as a basis.
  $form = file_field_instance_settings_form($field, $instance);

  // Remove the description option.
  unset($form['description_field']);

  return $form;
}

/**
 * Implements hook_field_load().
 */
function resolution_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {
  file_field_load($entity_type, $entities, $field, $instances, $langcode, $items, $age);
}

/**
 * Implements hook_field_prepare_view().
 */
function resolution_field_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items) {
}

/**
 * Implements hook_field_presave().
 */
function resolution_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_presave($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_insert().
 */
function resolution_field_insert($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_insert($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_update().
 */
function resolution_field_update($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_update($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_delete().
 */
function resolution_field_delete($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_delete($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_delete_revision().
 */
function resolution_field_delete_revision($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_delete_revision($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_is_empty().
 */
function resolution_field_is_empty($item, $field) {
  return file_field_is_empty($item, $field);
}

/**
 * Implements hook_field_widget_info().
 */
function resolution_field_widget_info() {
  return array(
    'resolution_resolution' => array(
      'label' => t('Resolution'),
      'field types' => array('resolution'),
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_settings_form().
 */
function resolution_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  // Use the file widget settings form.
  $form = file_field_widget_settings_form($field, $instance);

  return $form;
}

/**
 * Implements hook_field_widget_form().
 */
function resolution_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // Add display_field setting to field because file_field_widget_form() assumes it is set.
  $field['settings']['display_field'] = 1;

  $elements = file_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);
  $settings = $instance['settings'];

  foreach (element_children($elements) as $delta) {
    // Add all extra functionality provided by the resolution widget.
    $elements[$delta]['#process'][] = 'resolution_field_widget_process';
  }

  return $elements;
}

/**
 * An element #process callback for the resolution_resolution field type.
 *
 * Expands the resolution_resolution type to include the alt and title fields.
 */
function resolution_field_widget_process($element, &$form_state, $form) {
  $item = $element['#value'];
  $item['fid'] = $element['fid']['#value'];

  $instance = field_widget_instance($element, $form_state);

  $settings = $instance['settings'];
  $widget_settings = $instance['widget']['settings'];

  $element['#theme'] = 'resolution_widget';
  $element['#attached']['css'][] = drupal_get_path('module', 'file') . '/file.css';
  $element['#attached']['js'][] = drupal_get_path('module', 'file') . '/file.js';

  // Add the additional resolution information fields.
  $element['number'] = array(
    '#type' => 'textfield',
    '#title' => t('Resolution Number'),
    '#default_value' => isset($item['number']) ? $item['number'] : '',
    '#maxlength' => 128,
    '#weight' => -4,
    '#access' => (bool) $item['fid'],
  );
  $element['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Resolution Title'),
    '#default_value' => isset($item['title']) ? $item['title'] : '',
    '#maxlength' => 255,
    '#weight' => -3,
    '#access' => (bool) $item['fid'],
  );

  $element['status_type'] = array(
    '#type' => 'select',
    '#title' => t('Resolution Status'),
    '#default_value' => isset($item['status_type']) ? $item['status_type'] : '',
    '#options' => _resolution_resolution_types(),
    '#weight' => -2,
    '#access' => (bool) $item['fid'],
  );

  $element['status_notes'] = array(
    '#type' => 'textfield',
    '#title' => t('Resolution Status Notes'),
    '#default_value' => isset($item['status_notes']) ? $item['status_notes'] : '',
    '#maxlength' => 255,
    '#weight' => -1,
    '#access' => (bool) $item['fid'],
  );

  return $element;
}

/**
 * Returns HTML for an resolution field widget.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: A render element representing the resolution field widget.
 *
 * @ingroup themeable
 */
function theme_resolution_widget($variables) {
  $element = $variables['element'];
  $output = '';
  $output .= '<div class="resolution-widget form-managed-file clearfix">';

  $output .= '<div class="resolution-widget-data">';
  if ($element['fid']['#value'] != 0) {
    $element['filename']['#markup'] .= ' <span class="file-size">(' . format_size($element['#file']->filesize) . ')</span> ';
  }
  $output .= drupal_render_children($element);
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

/**
 * Implements hook_field_formatter_info().
 */
function resolution_field_formatter_info() {
  $formatters = array(
    'resolution' => array(
      'label' => t('Resolution'),
      'field types' => array('resolution'),
    ),
    'resolution_flat' => array(
      'label' => t('Resolution (flat)'),
      'field types' => array('resolution'),
    ),
    'resolution_url_plain' => array(
      'label' => t('URL to resolution'),
      'field types' => array('resolution'),
    ),
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function resolution_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  return array();
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function resolution_field_formatter_settings_summary($field, $instance, $view_mode) {
  return '';
}

/**
 * Implements hook_field_formatter_view().
 */
function resolution_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'resolution_flat':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#theme' => 'resolution_link',
          '#file' => (object) $item,
          '#status_type' => TRUE,
        );
      }
      break;

    case 'resolution_url_plain':
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => empty($item['uri']) ? '' : file_create_url($item['uri']));
      }
      break;

    case 'resolution':
      $types = _resolution_resolution_types();

      foreach ($types as $type => $typename) {
        $resolutions[$type] = array(
          '#theme' => 'resolution_group',
          '#title' => $typename,
        );
        hide($resolutions[$type]);
      }

      foreach ($items as $delta => $item) {
        $type = $item['status_type'];
        $resolutions[$type]['#items'][] = array(
          '#theme' => 'resolution_link',
          '#file' => (object) $item,
        );
        show($resolutions[$type]);
      }
      $resolutions = array_values($resolutions);

      // Giant HACK.
      $element[0] = array('#markup' => drupal_render($resolutions));
      break;
  }
  // dpm($element);
  return $element;
}

/**
 * Returns HTML for a resolution grouped by a specific type.
 *
 * @param $variables
 *   An associative array containing:
 *   - items: An associative array containg renderable elements.
 */
function theme_resolution_group($variables) {
  $items = $variables['items'];
  $title = $variables['title'];

  $output = '<dl class="resolution-type">';
  $output .= '<dt>' . $title . '</dt>';
  foreach (element_children($items) as $key) {
    $output .= '<dd>' . drupal_render($items[$key]) . '</dd>';
  }
  $output .= '</dl>';

  return $output;
}